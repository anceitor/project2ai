﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2ai
{
    public class AgentState
    {
        public string Statename;
        public int endstate { get; set; }
        public int moveCount { get; set; }
        public int stateCount { get; set; }
        public List<MoveTracking> statemoves { get; set; }
        public int visits = 0;
        public float policy { get; set; }
        public float reward { get; set; }
        

        public AgentState(int endstate, int move, int state)
        {
            this.endstate = endstate;
            moveCount = move;
            stateCount = state;
            statemoves = new List<MoveTracking>();
            policy = 0;
            for (int i = 0; i < move; i++)
            {
                statemoves.Add(new MoveTracking(stateCount));
            }
        }

        public void recordVisit(int location, int move)
        {
            visits++;
            statemoves[move].visit(location);
        }

        public void calulateMoveodds()
        {
            foreach (var item in statemoves)
            {
                item.calulateVisitPercents();
            }
        }

        public float getMax(TraversalAgent agent)
        {
            float max = -1000000;
            foreach (var action in statemoves)
            {
                float current = 0;
                for (int i = 0; i < action.finalPrecentages.Count; i++)
                {
                    current += action.finalPrecentages[i] * agent.States[i].policy;
                }
                if (max < current)
                {
                    max = current;
                }
            }
            return max;
        }
    }
}
