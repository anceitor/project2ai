﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2ai
{
    public static class FileReader
    {
        public static Maze CreateMaze(string fileName)
        {
            string lineRead;
            string path = "../../" + fileName;

            Maze maze = new Maze();

            if (!File.Exists(path)) return maze;

            StreamReader file = new StreamReader(path);
            int move;
            int states;
            float gamma;

            Int32.TryParse(file.ReadLine(),out move);
            Int32.TryParse(file.ReadLine(), out states);
            float.TryParse(file.ReadLine(), out gamma);

            maze.moves = move;
            maze.statesAmount = states;
            maze.gamma = gamma;

            while ((lineRead = file.ReadLine()) != null)
            {
                float reward;
                int end;
                string location;
                float.TryParse(lineRead, out reward);
                Int32.TryParse(file.ReadLine(), out end);
                location = file.ReadLine();
                maze.addState(location, reward, end);
            }


            return maze;
        }
    }
}
