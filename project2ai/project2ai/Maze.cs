﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2ai
{
    public class Maze
    {
        public int moves { get; set; }
        public int statesAmount { get; set; }
        public float gamma { get; set; }
        public List<State> states { get; set; }

        public Maze()
        {
            states = new List<State>();
        }

        public void addState(string file_location, float reward, int isendstate)
        {
            string lineRead;
            string path = "../../states/" + file_location;

            List<string> state = new List<string>();
            State newstate = new State();
            if (isendstate != 1)
            {
                if (!File.Exists(path))
                    return;

                StreamReader file = new StreamReader(path);


                while ((lineRead = file.ReadLine()) != null)
                {
                    state.Add(lineRead);
                }

                newstate.addMoveAndStateCount(state[0]);

                for (int i = 1; i < state.Count; i++)
                {
                    newstate.addaction(state[i]);
                }
                newstate.policy = 0;
            }
            else
            {
                newstate.policy = reward;
            }
            newstate.reward = reward;
            newstate.endstate = isendstate;
            states.Add(newstate);

            return;
        }

        public void findOptimalpolicies(int generations)
        {
            if (states.Count==0) return;
            for (int i = 0; i < generations; i++)
            {
                //Console.WriteLine("generation " + (i + 1));
                List<float> newPolicy = new List<float>();
                foreach (var item in states)
                {
                    if (item.actions.Count == 0)
                    {
                        newPolicy.Add(item.policy);
                        continue;
                    }
                    item.policy = item.reward + (gamma * item.getMax(this));
                }
            }

        }

    }
}
