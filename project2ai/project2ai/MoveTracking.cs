﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2ai
{
    public class MoveTracking
    {
        public List<int> moveCount;
        public List<float> finalPrecentages;
        public int timesVisited =1;

        public MoveTracking(int statenum)
        {
            moveCount = new List<int>();
            finalPrecentages = new List<float>();

            for (int i = 0; i < statenum; i++)
            {
                moveCount.Add(0);
                finalPrecentages.Add(0);
            }
        }

        public void visit(int location)
        {
            timesVisited++;
            moveCount[location] += 1;
        }

        public void calulateVisitPercents()
        {
            for (int i = 0; i < moveCount.Count; i++)
            {
                float percent = ((float)moveCount[i] / (float)timesVisited);

                finalPrecentages[i] = percent;
            }
        }
    }
}
