using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2ai
{
    class Program
    {
        static void Main(string[] args)
        {
            Maze mymaze = FileReader.CreateMaze("reinforcement_maze.txt");

            //performing Value iteration
            mymaze.findOptimalpolicies(50);

            Console.WriteLine("Value Iteration \n");
            foreach (var item in mymaze.states)
            {
                Console.WriteLine(item.policy);
            }

            Console.WriteLine("\nActive Reinforcement Learning \n");
            //creating agents
            TraversalAgent newagent = new TraversalAgent(mymaze);
            //training agents
            newagent.TrainAgent(100000);
            //using values to find optimal polich
            newagent.findAgentsOptimalpolicies(100000);

            Console.WriteLine("Result matrices I calculated for each state \n");
            newagent.printresults();
            Console.WriteLine("optimal Policy for each state \n");
            foreach (var item in newagent.AgentStates)
            {
                Console.WriteLine(item.Statename);
                Console.WriteLine(item.policy);
            }
        }
    }
}
