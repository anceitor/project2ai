﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2ai
{
    public class TraversalAgent
    {
        public List<State> States { get; set; }
        public List<AgentState> AgentStates { get; set; }
        public int AgentsDeployed;
        public float gamma;
        

        public TraversalAgent (Maze maze)
        {
            States = maze.states;
            gamma = maze.gamma;
            initateAgentStates(States.Count());
        }

        public void TrainAgent(int training_generations)
        {
            for (int i = 0; i < training_generations; i++)
            {
                int count = 0;
                AgentsDeployed++;
                int currState = RandomNumber.Between(0, States.Count - 1);
                while (States[currState].endstate != 1)
                {
                    count++;
                    float rand = RandomNumber.Between(1, 100);
                    float movechance = rand/100;
                    int moveDirection = RandomNumber.Between(0, States[0].moveCount - 1);

                    int nextState = States[currState].getNextState(movechance, moveDirection);
                    AgentStates[currState].recordVisit(nextState, moveDirection);

                    currState = nextState;

                    if (count > 1000) break;
                }

            }
            foreach (var item in AgentStates)
            {
                item.calulateMoveodds();
            }
        }

        public void initateAgentStates(int count)
        {
            AgentStates = new List<AgentState>();
            for (int i = 0; i < count; i++)
            {
                AgentStates.Add(new AgentState(States[i].endstate, States[i].moveCount, States[i].stateCount));
                AgentStates[i].Statename = "State " + (i+1);
                AgentStates[i].reward = States[i].reward;
            }
        }

        public void printresults()
        {
            foreach (var item in AgentStates)
            {
                Console.WriteLine(item.Statename);
                foreach (var items in item.statemoves)
                {
                    string toprint ="";
                    foreach (var a in items.finalPrecentages)
                    {
                        toprint += Math.Round(a, 1, MidpointRounding.ToEven) + " ";
                    }
                    Console.WriteLine(toprint);

                }
                Console.WriteLine('\n');
            }
        }

        public void findAgentsOptimalpolicies(int generations)
        {
            if (AgentStates.Count == 0) return;
            for (int i = 0; i < generations; i++)
            {
                //Console.WriteLine("generation " + (i + 1));
                List<float> newPolicy = new List<float>();
                foreach (var item in AgentStates)
                {
                    if (item.endstate == 1)
                    {
                        item.policy = item.reward;
                        continue;
                    }
                    item.policy = item.reward + (gamma * item.getMax(this));
                }
            }

        }

    }
}
