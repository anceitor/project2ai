﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2ai
{
    public class action
    {
        public List<float> probabilities { get; set; }
        public int count = 1;

        public action()
        {
            probabilities = new List<float>();
        }

        public int MoveState(float movechance)
        {

            return 1;
        }

        public void createAction(string probs)
        {
            float x;
            string[] strlist = probs.Split(' ');
            foreach (String item in strlist)
            {
                float.TryParse(item,out x);
                probabilities.Add(x);
            }
        }

        public int getState(float chance)
        {
            float addupchance =0;
            for (int i = 0; i < probabilities.Count; i++)
            {
                addupchance += probabilities[i];
                if (addupchance >= chance)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
