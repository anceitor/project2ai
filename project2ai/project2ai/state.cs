﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2ai
{
    public class State
    {
        public float reward { get; set; }
        public int endstate { get; set; }
        public int moveCount { get; set; }
        public int stateCount { get; set; }
        public List<action> actions { get; set; }
        public float policy { get; set; }

        //Properties used in the agent
        public int timesVisited { get; set; }


        public State()
        {
            actions = new List<action>();
            timesVisited = 1;
        }

        public void addaction(string action)
        {
            action newaction = new action();
            newaction.createAction(action);
            actions.Add(newaction);
        }

        public void addMoveAndStateCount(string move_and_state_count)
        {
            string[] strlist = move_and_state_count.Split(' ');
            int x;
            int.TryParse(strlist[0], out x);
            moveCount = x;
            int.TryParse(strlist[1], out x);
            stateCount = x;

        }

        public float getMax(Maze maze)
        {
            float max = -1000000;
            foreach (var action in actions)
            {
                float current = 0;
                for (int i = 0; i < action.probabilities.Count; i++)
                {
                    current += action.probabilities[i] * maze.states[i].policy;
                }
                if (max<current)
                {
                    max = current;
                }
            }
            return max;
        }

        public int getNextState(float chance, int move)
        {
            return actions[move].getState(chance);
        }
    }
}
